import flask, flask.views
import utils

class Dashboard(flask.views.MethodView):
    @utils.login_required
    def get(self):
        user = flask.session['username']
        flask.flash(user)
        return flask.render_template('dashboard.html')

    def post(self):
        if 'logout' in flask.request.form:
            flask.session.pop('username', None)
            return flask.redirect(flask.url_for('index'))