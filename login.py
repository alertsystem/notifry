import flask, flask.views
import database
import utils


class Login(flask.views.MethodView):
    @utils.nocache
    def get(self):
        return flask.render_template('index.html')

    def post(self):
        if 'register' in flask.request.form:
            name = flask.request.form['name']
            email1 = flask.request.form['email1']
            email2 = flask.request.form['email2']
            passwd1 = flask.request.form['password1']
            passwd2 = flask.request.form['password2']
            database.signup(name, email1, passwd1)
            return flask.redirect(flask.url_for('index'))

        if 'login' in flask.request.form:
            required = ['email', 'passwd']
            for r in required:
                if r not in flask.request.form:
                    flask.flash("Error: {0} is required.".format(r))
                    return flask.redirect(flask.url_for('index'))
                email = flask.request.form['email']
                passwd = flask.request.form['passwd']
                users = database.login(email, passwd)
                if email in users and users[email] == passwd:
                    flask.session['username'] = email
                else:
                    flask.flash("Username doesn't exist or incorrect password")
                return flask.redirect(flask.url_for('dashboard'))
