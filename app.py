import flask
import settings
import utils

# Views
from main import Main
from login import Login
from dashboard import Dashboard

app = flask.Flask(__name__)
app.secret_key = settings.secret_key

# Routes
#app.add_url_rule('/<page>/',
#                 view_func=Main.as_view('page'),
#                 methods=["GET"])
app.add_url_rule('/',
                 view_func=Login.as_view('index'),
                 methods=["GET", "POST"])
app.add_url_rule('/dashboard/',
                 view_func=Dashboard.as_view('dashboard'),
                 methods=['GET', 'POST'])

@app.errorhandler(404)
def page_not_found(error):
    return flask.render_template('404.html'), 404

app.debug = True
app.run()